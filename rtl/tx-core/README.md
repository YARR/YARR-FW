## Trigger Tagging

Trigger tagging is added in this update. See the ILA output for both the pulse generator (1) and the trigger tagging generator (2), for a trigger window size of 7 bunch crossings:

![](/doc/ILA_7triggers_fixed_tag_code_gen.png)
![](/doc/ILA_7triggers_fixed_pulse_extender.png)

Ideal behavior:
- Consecutive trigger tags 

### 