----------------------------------------------------------------------------
--  Project : Yarr
--  File    : trig_extender.vhd
--  Author  : Lucas Cendes
--  E-Mail  : lucascendes@gmail.com
--  Comments: Extends a trigger for the specified number of cycles
----------------------------------------------------------------------------
library ieee;
use     ieee.std_logic_1164.all;
use     ieee.std_logic_misc.all;
use     ieee.numeric_std.all;

entity pulse_extender is 
    generic (
        g_INTERVAL_WIDTH : integer := 32
    );
    port (
        clk_i          : in  std_logic;
        rst_n_i        : in  std_logic;

        pulse_i        : in std_logic; 
        ext_interval_i : in std_logic_vector (g_INTERVAL_WIDTH-1 downto 0);

        ext_pulse_o    : out std_logic
    );
end pulse_extender;

architecture behavioral of pulse_extender is

    signal cycle_counter : unsigned (g_INTERVAL_WIDTH-1 downto 0);
    signal trig_ready    : std_logic;
    signal ext_pulse     : std_logic;

    -- COMPONENT ila_rx_dma_wb
    -- PORT (
    --     clk : IN STD_LOGIC;
    --     probe0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0); 
    --     probe1 : IN STD_LOGIC_VECTOR(63 DOWNTO 0); 
    --     probe2 : IN STD_LOGIC_VECTOR(63 DOWNTO 0); 
    --     probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
    --     probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
    --     probe5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
    --     probe6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
    --     probe7 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
    --     probe8 : IN STD_LOGIC_VECTOR(31 DOWNTO 0); 
    --     probe9 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
    --     probe10 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --     probe11 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
    -- );
    -- END COMPONENT  ;
begin

    ----------------------------------------------------------------------------
    -- Counter that keeps track of the number of cycles in which the ouput
    -- should be held high. The value of the counter is set whenever the current
    -- value of the counter is 0 and a trigger pulse is received
    -- 
    -- TODO: figure out handling of the case where a trigger pulse is received 
    --   during the readout of a different trigger pulse. this has a few
    --   solutions:
    --    - Ignore, unless at the absolute end of cycle (CURRENT and matching
    --      previous behavior)
    --    - Double the number of output BCs, to maintain total BCs divisible by
    --      trigger window
    --    - Reset number of output BCs and continue reading out, which WILL
    --      cause a different number of triggers than the pre set window
    ----------------------------------------------------------------------------
    pr_cycle_counter : process (rst_n_i, clk_i)
    begin

        if (rst_n_i = '0') then
            cycle_counter <= (others => '0');
            trig_ready <= '0';
            ext_pulse <= '0';
        elsif rising_edge(clk_i) then
            
            -- Set up extender length
            if (cycle_counter = 0) then
                if (pulse_i = '1') then
                    cycle_counter <= unsigned(ext_interval_i);
                end if;
            else
                cycle_counter <= cycle_counter - 1;
            end if;                
            
            -- Delayed pulse to match cycle counter
            if (pulse_i = '1') then
                trig_ready <= '1';
            else
                trig_ready <= '0';
            end if;

            -- If cycle counter is greater than 0, send pulse
            if (cycle_counter > 0) then
                ext_pulse <= '1';
            -- Also send a pulse if trigger_ready (PREVIOUS CODE HOLDOVER)
            else
                ext_pulse <= trig_ready;
            end if;
        end if;

    end process;
    
    ext_pulse_o <= ext_pulse;

    -- aurora_lane_debug : ila_rx_dma_wb
    -- PORT MAP (
    --     clk => clk_i,
    --     probe0 => (others => '0'), 
    --     probe1(63 downto g_INTERVAL_WIDTH) => (others => '0'),
    --     probe1(g_INTERVAL_WIDTH-1 downto 0) => std_logic_vector(ext_interval_i),
    --     probe2(63 downto g_INTERVAL_WIDTH) => (others => '0'),
    --     probe2(g_INTERVAL_WIDTH-1 downto 0) => std_logic_vector(cycle_counter),
    --     probe3(0) => ext_pulse,
    --     probe4(0) => trig_ready, 
    --     probe5(0) => pulse_i,
    --     probe6(0) => '0',
    --     probe7(0) => '0',
    --     probe8 => (others => '0'),
    --     probe9(0) => clk_i,
    --     probe10(0) => '0',
    --     probe11(0) => '0'
    -- );

end behavioral;