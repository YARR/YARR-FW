----------------------------------------------------------------------------
--  Project : Yarr
--  File    : trig_code_gen.vhd
--  Author  : Lauren Choquer
--  E-Mail  : choquerlauren@gmail.com
--  Comments: Converts trigger pulses into RD53A trig encoding
----------------------------------------------------------------------------
library ieee;
use     ieee.std_logic_1164.all;
use     ieee.std_logic_misc.all;
use     ieee.numeric_std.all;

entity trig_code_gen is 
    port (
        clk_i        : in  std_logic;
        rst_n_i      : in  std_logic;

        enable_i     : in  std_logic;
        pulse_i      : in  std_logic;

        code_o       : out std_logic_vector(31 downto 0);  -- two 16-bit code words
        code_ready_o : out std_logic

        );
end trig_code_gen;

----------------------------------------------------------------------------
-- Architecture for synthesis
----------------------------------------------------------------------------
architecture behavioral of trig_code_gen is

    -- Trigger encoding. Converts 4-bit pattern into an 8-bit code
    component trig_encoder
        port (
            pattern_i   : in  std_logic_vector(3 downto 0);
            code_o      : out std_logic_vector(7 downto 0)  
        );
    end component;

    -- Tag encoding. Converts 6-bit base tag into an 8-bit code
    component tag_encoder
        port (
            base_tag_i  : in unsigned(5 downto 0);
            code_o      : out std_logic_vector(7 downto 0)
          );
    end component;



    constant c_IDLE_WORD    : std_logic_vector(15 downto 0) := x"aaaa";

    signal trig_cntr        : unsigned(1 downto 0);
    signal command_cntr     : unsigned(1 downto 0);
    signal trig_count_ready : std_logic; -- 1 BC clock
    signal command_count_ready : std_logic; -- 4 BC clock

    signal trig_sreg        : std_logic_vector(3 downto 0);
    signal trig_word        : std_logic_vector(3 downto 0);
    signal trig_word_ready : std_logic;
    signal trig_bit         : std_logic;

    signal command_sreg     : std_logic_vector(3 downto 0);
    signal command_word     : std_logic_vector(3 downto 0);
    signal command_word_ready : std_logic;
    signal trig_encoding    : std_logic_vector(7 downto 0); 

    constant c_MAX_TAG      : integer := 53;
    signal base_tag         : unsigned(5 downto 0);
    signal tag_encoding     : std_logic_vector(7 downto 0);

    signal code_word        : std_logic_vector (15 downto 0);
    signal first_word       : std_logic_vector (15 downto 0);
    signal first_word_done  : std_logic;

    signal code_s           : std_logic_vector (31 downto 0);
    signal code_ready_s     : std_logic;

begin

    trig_bit <= or_reduce(trig_word);  -- Computes an OR of all bits in trig_word

    ----------------------------------------------------------------------------
    -- Increment counters for bunch crossing (4) and reg filling (8)
    -- Counter wrap to zero is inferred not explicit.
    -- Shift trig_bit into command_sreg at the end of each bunch crossing
    -- Shift pulse_i into trig_sreg at the end of each 4 clock cycles
    ----------------------------------------------------------------------------
    pr_trig_code : process (rst_n_i, clk_i)
    begin

        if (rst_n_i = '0') then
            
            trig_cntr <= (others=>'0');
            trig_sreg <= (others=>'0');
            trig_count_ready <= '0';
            trig_word <= (others=>'0');

            command_cntr <= (others=>'0');
            command_sreg <= (others=>'0');
            command_count_ready <= '0';
            command_word <= (others=>'0');

            base_tag <= (others => '0');

            first_word <= c_IDLE_WORD;
            first_word_done <= '0';
            code_s <= c_IDLE_WORD & c_IDLE_WORD;
            code_ready_s <= '0';

        elsif rising_edge(clk_i) then
            trig_cntr <= trig_cntr + 1;
            trig_sreg <= trig_sreg(2 downto 0) & pulse_i;

            -- set trig ready signal at the end of 1 BC
            if (trig_cntr = "11") then
                trig_count_ready <= '1';
            else
                trig_count_ready <= '0';
            end if;

            -- update command counter/shift register every 1 BC, as well as set command ready signal at the end of 4 BC
            if (trig_count_ready = '1') then
                command_cntr <= command_cntr + 1;
                command_sreg <= trig_bit & command_sreg(3 downto 1);
                if (command_cntr = "11") then
                    command_count_ready <= '1';
                else
                    command_count_ready <= '0';
                end if;
            end if;

            -- IF: update trigger word if trig count is HIGH and set trig_word signal to HIGH
            -- ELSE: maintain and set word_ready to 0
            if (trig_count_ready = '1') then
                trig_word <= trig_sreg;
                trig_word_ready <= '1';
            else
                trig_word <= trig_word;
                trig_word_ready <= '0';
            end if;

            -- IF: update command word if command count is HIGH and set command count signal to HIGH
            -- ELSE: maintain and set word_ready to 0
            if (command_count_ready = '1') then
                command_word <= command_sreg;
                command_word_ready <= '1';
            else
                command_word <= command_word;
                command_word_ready <= '0';
            end if;

            -- WHEN command and trigger words are ready...
            if (command_word_ready = '1' and trig_word_ready = '1') then
                if (command_word = "0000") then
                    code_word <= c_IDLE_WORD;
                else
                    -- ... update code word
                    code_word <= trig_encoding & tag_encoding;

                    -- update base tag
                    if (base_tag < c_MAX_TAG) then
                        base_tag <= base_tag + 1;
                    else 
                        base_tag <= (others => '0');
                    end if;
                end if;
                
                -- update first word
                if (first_word_done = '1') then
                    first_word <= first_word;
                    first_word_done <= '0';
                else
                    first_word <= code_word;
                    first_word_done <= '1';
                end if;

            else
                -- otherwise, maintain first word + signals
                first_word <= first_word;
                first_word_done <= first_word_done;
            end if;

            -- Update output signals based on first word logic
            if (first_word_done = '1') then 
                code_s <= first_word & code_word;

                if (first_word = c_IDLE_WORD and code_word = c_IDLE_WORD) then
                    code_ready_s <= '0';
                else
                    code_ready_s <= enable_i;
                end if;
            else
                code_s <= code_s;
                code_ready_s <= code_ready_s;
            end if;
        end if;
        
    end process;
    
    -- Set the output signals
    code_o <= code_s;
    code_ready_o <= code_ready_s;
   
    ----------------------------------------------------------------------------
    --  Encode upper 4 bits of command_word
    -- ** Warning : code_o is a block output from this asychronous block **
    ----------------------------------------------------------------------------
    cmp_trig_encoder : trig_encoder 
    port map (
        pattern_i   => command_word,
        code_o      => trig_encoding
    );

    cmp_tag_encoder : tag_encoder
    port map (
        base_tag_i  => base_tag,
        code_o      => tag_encoding
    );

end behavioral;
