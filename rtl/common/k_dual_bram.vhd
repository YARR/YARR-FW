----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/11/2017 11:39:54 AM
-- Design Name: 
-- Module Name: k_dual_bram - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

Library UNISIM;
use UNISIM.vcomponents.all;

Library UNIMACRO;
use UNIMACRO.vcomponents.all;

entity k_dual_bram is
    Port ( 
    -- SYS CON
    clk_i            : in std_logic;
    rst_i            : in std_logic;
    
    -- Wishbone Slave in
    wba_adr_i            : in std_logic_vector(32-1 downto 0);
    wba_dat_i            : in std_logic_vector(64-1 downto 0);
    wba_we_i            : in std_logic;
    wba_stb_i            : in std_logic;
    wba_cyc_i            : in std_logic; 
    
    -- Wishbone Slave out
    wba_dat_o            : out std_logic_vector(64-1 downto 0);
    wba_ack_o            : out std_logic;
           
    -- Wishbone Slave in
    wbb_adr_i            : in std_logic_vector(32-1 downto 0);
    wbb_dat_i            : in std_logic_vector(64-1 downto 0);
    wbb_we_i            : in std_logic;
    wbb_stb_i            : in std_logic;
    wbb_cyc_i            : in std_logic; 
    
    -- Wishbone Slave out
    wbb_dat_o            : out std_logic_vector(64-1 downto 0);
    wbb_ack_o            : out std_logic; 
    
    bram_busy_o          : out std_logic;
    bram_diff_cnt_rst_i  : in std_logic;
    bram_ack_cnt_o       : out unsigned(31 downto 0);
    bram_streams_in      : out unsigned(31 downto 0);
    bram_streams_out     : out unsigned(31 downto 0);
    bram_packages_in      : out unsigned(31 downto 0);
    bram_packages_out     : out unsigned(31 downto 0);
    bram_almost_full_i   : in unsigned(31 downto 0);
    bram_almost_empty_i  : in unsigned(31 downto 0)

           );
end k_dual_bram;

architecture Behavioral of k_dual_bram is

    constant BLOCK_ADDR_WIDTH_C : integer := 13;
    constant DATA_WIDTH_C : integer := 64;
    constant BLOCK_ROW_C : integer := 32;
    constant BLOCK_COL_EXP_C : integer := 4;
    constant BLOCK_COL_C : integer := 2**BLOCK_COL_EXP_C;
    constant BLOCK_DATA_WIDTH_C : integer := DATA_WIDTH_C/BLOCK_ROW_C;
    
    constant BRAM_ADDR_LIMIT_C : unsigned(31 downto 0) := TO_UNSIGNED(131072, 32);
    -- constant BRAM_BUSY_CNT_C : unsigned(31 downto 0) := TO_UNSIGNED(117964, 32); -- 90% BRAM space
    -- constant BRAM_ALMOST_EMPTY_C : unsigned(31 downto 0) := TO_UNSIGNED(65536, 32); -- 50% BRAM space
    constant MAX_ADDR_DIFF_C : unsigned(31 downto 0) := x"0FFFFFFF"; -- Half of 29 bit max
    signal WEA_S : std_logic_vector(0 downto 0);
    signal WEB_S : std_logic_vector(0 downto 0);
    
    signal selecta_s : std_logic_vector (BLOCK_COL_EXP_C-1 downto 0);
    signal selectb_s : std_logic_vector (BLOCK_COL_EXP_C-1 downto 0);
    signal selecta_s_2 : std_logic_vector (BLOCK_COL_EXP_C-1 downto 0);
    signal selectb_s_2 : std_logic_vector (BLOCK_COL_EXP_C-1 downto 0);
    
    type ram_data_bus is array (BLOCK_COL_C-1 downto 0) of std_logic_vector(DATA_WIDTH_C-1 downto 0);
    signal wba_dat_a : ram_data_bus;
    signal wbb_dat_a : ram_data_bus;
    
    signal wba_cyc_v_s            : std_logic_vector(BLOCK_COL_C-1 downto 0);
    signal wbb_cyc_v_s            : std_logic_vector(BLOCK_COL_C-1 downto 0);
    
    -- Wishbone Slave in
    signal wba_adr_s            : std_logic_vector(32-1 downto 0);
    signal wba_dat_i_s            : std_logic_vector(64-1 downto 0);
    signal wba_we_s            : std_logic;
    signal wba_stb_s            : std_logic;
    signal wba_cyc_s            : std_logic; 
    
    signal wba_dat_o_s            : std_logic_vector(64-1 downto 0);
    signal wba_ack_s            : std_logic;
    signal wba_ack_s_2          : std_logic;
    signal wba_ack_s_3          : std_logic;
    
    signal wbb_adr_s            : std_logic_vector(32-1 downto 0);
    signal wbb_dat_i_s            : std_logic_vector(64-1 downto 0);
    signal wbb_we_s            : std_logic;
    signal wbb_stb_s            : std_logic;
    signal wbb_cyc_s            : std_logic;
    
    signal wbb_dat_o_s            : std_logic_vector(64-1 downto 0);    
    signal wbb_ack_s            : std_logic;
    signal wbb_ack_s_2          : std_logic;
    signal wbb_ack_s_3          : std_logic;

    constant c_ALL_ZEROS : unsigned(31 downto 0) := (others => '0');
    constant c_ALL_ONES : unsigned(31 downto 0) := (others => '1');

    signal ack_cnt : unsigned(31 downto 0);
    
    signal wba_adr_s_2            : std_logic_vector(32-1 downto 0);
    signal wba_dat_i_s_2          : std_logic_vector(64-1 downto 0);
    signal wba_we_s_2             : std_logic;
    signal wba_stb_s_2            : std_logic;
    signal wba_cyc_s_2            : std_logic;
    signal wbb_adr_s_2            : std_logic_vector(32-1 downto 0);
    signal wbb_dat_i_s_2          : std_logic_vector(64-1 downto 0);
    signal wbb_we_s_2             : std_logic;
    signal wbb_stb_s_2            : std_logic;
    signal wbb_cyc_s_2            : std_logic;
    
    signal wba_adr_s_3            : std_logic_vector(32-1 downto 0);
    signal wba_dat_i_s_3          : std_logic_vector(64-1 downto 0);
    signal wba_we_s_3             : std_logic;
    signal wba_stb_s_3            : std_logic;
    signal wba_cyc_s_3            : std_logic;
    signal wbb_adr_s_3            : std_logic_vector(32-1 downto 0);
    signal wbb_dat_i_s_3          : std_logic_vector(64-1 downto 0);
    signal wbb_we_s_3             : std_logic;
    signal wbb_stb_s_3            : std_logic;
    signal wbb_cyc_s_3            : std_logic;
    
    signal wba_dat_o_s_2            : std_logic_vector(64-1 downto 0);
    signal wbb_dat_o_s_2            : std_logic_vector(64-1 downto 0);
    
    attribute max_fanout : integer;
    attribute max_fanout of wba_adr_s : signal is 8;
    attribute max_fanout of wba_we_s : signal is 8;
    attribute max_fanout of wba_dat_i_s : signal is 8;
    attribute max_fanout of wbb_adr_s : signal is 8;
    attribute max_fanout of wbb_dat_i_s : signal is 8;
    attribute max_fanout of wbb_we_s : signal is 8;
    
    attribute max_fanout of wba_adr_s_2 : signal is 8;
    attribute max_fanout of wba_we_s_2 : signal is 8;
    attribute max_fanout of wba_dat_i_s_2 : signal is 8;
    attribute max_fanout of wbb_adr_s_2 : signal is 8;
    attribute max_fanout of wbb_dat_i_s_2 : signal is 8;
    attribute max_fanout of wbb_we_s_2 : signal is 8;
    
    attribute max_fanout of wba_adr_s_3 : signal is 8;
    attribute max_fanout of wba_we_s_3 : signal is 8;
    attribute max_fanout of wba_dat_i_s_3 : signal is 8;
    attribute max_fanout of wbb_adr_s_3 : signal is 8;
    attribute max_fanout of wbb_dat_i_s_3 : signal is 8;
    attribute max_fanout of wbb_we_s_3 : signal is 8;

    -- Debug things
    signal bram_busy : std_logic := '0';
    signal bram_busy_adr : std_logic := '0';
    signal roll_a : std_logic := '0';
    signal roll_b : std_logic := '0';


    signal wba_adr_latch : std_logic_vector(31 downto 0);
    signal wbb_adr_latch : std_logic_vector(31 downto 0);
    signal adr_diff : unsigned(31 downto 0);
    signal adr_diff_a : unsigned(31 downto 0);
    signal adr_diff_b : unsigned(31 downto 0);

    signal wba_valid_streams : unsigned(31 downto 0);
    signal wbb_valid_streams : unsigned(31 downto 0);

    signal wba_packages : unsigned(31 downto 0);
    signal wbb_packages : unsigned(31 downto 0);
    
    component ila_trigtag 
        port (
            clk : in std_logic;
            probe0 : in std_logic_vector(31 downto 0); 
            probe1 : in std_logic_vector(31 downto 0); 
            probe2 : in std_logic_vector(31 downto 0); 
            probe3 : in std_logic_vector(31 downto 0); 
            probe4 : in std_logic_vector(31 downto 0);
            probe5 : in std_logic_vector(63 downto 0);
            probe6 : in std_logic_vector(15 downto 0);
            probe7 : in std_logic_vector(0 downto 0);
            probe8 : in std_logic_vector(0 downto 0);
            probe9 : in std_logic_vector(0 downto 0);
            probe10 : in std_logic_vector(0 downto 0);
            probe11 : in std_logic_vector(0 downto 0);
            probe12 : in std_logic_vector(0 downto 0);
            probe13 : in std_logic_vector(0 downto 0);
            probe14 : in std_logic_vector(0 downto 0);
            probe15 : in std_logic_vector(0 downto 0);
            probe16 : in std_logic_vector(0 downto 0);
            probe17 : in std_logic_vector(0 downto 0);
            probe18 : in std_logic_vector(0 downto 0);
            probe19 : in std_logic_vector(0 downto 0);
            probe20 : in std_logic_vector(0 downto 0)
        );
    end component;

begin
    -- bram_debug : ila_trigtag PORT MAP (
    --     clk => clk_i,
    --     probe0 => std_logic_vector(wba_adr_latch),
    --     probe1 => std_logic_vector(wbb_adr_latch),
    --     probe2 => std_logic_vector(adr_diff),
    --     probe3 => std_logic_vector(adr_diff_a),
    --     probe4 => std_logic_vector(adr_diff_b),
    --     probe5(63 downto 32) => wba_dat_o_s(31 downto 0),
    --     probe5(31 downto 0) => wbb_dat_i_s_3(31 downto 0),
    --     probe6(15 downto 8) => wba_dat_o_s(30 downto 23), -- 8 bit tags
    --     probe6(7 downto 0) => wbb_dat_i_s_3(30 downto 23), -- 8 bit tags
    --     probe7(0) => wba_ack_s_3,
    --     probe8(0) => wba_dat_o_s(31),
    --     probe9(0) => wbb_stb_s_3,
    --     probe10(0) => wbb_dat_i_s_3(31),
    --     probe11(0) => bram_busy,
    --     probe12(0) => bram_busy_adr, 
    --     probe13(0) => roll_a,
    --     probe14(0) => roll_b,
    --     probe15(0) => wba_stb_s_3,
    --     probe16(0) => wba_cyc_s_3,
    --     probe17(0) => wbb_stb_s_3,
    --     probe18(0) => wbb_cyc_s_3,
    --     probe19(0) => '0',
    --     probe20 => (others => '0')
    -- );
    
    --to improve the fanout
	input_delay: process (clk_i, rst_i)
	begin
		if (rst_i ='1') then
            wba_adr_s <= (others => '0');
            wba_dat_i_s <= (others => '0');
            wba_we_s <= '0';
            wba_stb_s <= '0';
            wba_cyc_s <= '0';
            wbb_adr_s <= (others => '0');
            wbb_dat_i_s <= (others => '0');
            wbb_we_s <= '0';
            wbb_stb_s <= '0';
            wbb_cyc_s <= '0';
            
            wba_adr_s_2 <= (others => '0');
            wba_dat_i_s_2 <= (others => '0');
            wba_we_s_2 <= '0';
            wba_stb_s_2 <= '0';
            wba_cyc_s_2 <= '0';
            wbb_adr_s_2 <= (others => '0');
            wbb_dat_i_s_2 <= (others => '0');
            wbb_we_s_2 <= '0';
            wbb_stb_s_2 <= '0';
            wbb_cyc_s_2 <= '0';
            
            wba_adr_s_3 <= (others => '0');
            wba_dat_i_s_3 <= (others => '0');
            wba_we_s_3 <= '0';
            wba_stb_s_3 <= '0';
            wba_cyc_s_3 <= '0';
            wbb_adr_s_3 <= (others => '0');
            wbb_dat_i_s_3 <= (others => '0');
            wbb_we_s_3 <= '0';
            wbb_stb_s_3 <= '0';
            wbb_cyc_s_3 <= '0';
            
		elsif (clk_i'event and clk_i = '1') then
            wba_adr_s <= wba_adr_i;
            wba_dat_i_s <= wba_dat_i;
            wba_we_s <= wba_we_i and wba_stb_i;
            wba_stb_s <= wba_stb_i;
            wba_cyc_s <= wba_cyc_i;
            wbb_adr_s(31 downto 29) <= "000";
            wbb_adr_s(28 downto 0) <= wbb_adr_i(28 downto 0);
            wbb_dat_i_s <= wbb_dat_i;
            wbb_we_s <= wbb_we_i and wbb_stb_i;
            wbb_stb_s <= wbb_stb_i;
            wbb_cyc_s <= wbb_cyc_i;
            
            wba_adr_s_2 <= wba_adr_s;
            wba_dat_i_s_2 <= wba_dat_i_s;
            wba_we_s_2 <= wba_we_s;
            wba_stb_s_2 <= wba_stb_s;
            wba_cyc_s_2 <= wba_cyc_s;
            wbb_adr_s_2 <= wbb_adr_s;
            wbb_dat_i_s_2 <= wbb_dat_i_s;
            wbb_we_s_2 <= wbb_we_s;
            wbb_stb_s_2 <= wbb_stb_s;
            wbb_cyc_s_2 <= wbb_cyc_s; 
            
            wba_adr_s_3 <= wba_adr_s_2;
            wba_dat_i_s_3 <= wba_dat_i_s_2;
            wba_we_s_3 <= wba_we_s_2;
            wba_stb_s_3 <= wba_stb_s_2;
            wba_cyc_s_3 <= wba_cyc_s_2;
            wbb_adr_s_3 <= wbb_adr_s_2;
            wbb_dat_i_s_3 <= wbb_dat_i_s_2;
            wbb_we_s_3 <= wbb_we_s_2;
            wbb_stb_s_3 <= wbb_stb_s_2;
            wbb_cyc_s_3 <= wbb_cyc_s_2;		
            
            

		end if;
		
	end process input_delay;

    bram_ack_cnt_o <= adr_diff;
    bram_busy_o <= bram_busy_adr; 
    -- bram_busy_o <= bram_busy;

	bram: process (clk_i, rst_i)
	begin
		if (rst_i ='1') then
			wba_ack_s <= '0';
            wbb_ack_s <= '0';
            ack_cnt <= (others => '0');
            bram_busy <= '0';
            bram_busy_adr <= '0';

            adr_diff <= (others => '0');
            adr_diff_a <= (others => '0');
            adr_diff_b <= (others => '0');

            wba_adr_latch <= (others => '0');
            wbb_adr_latch <= (others => '0');

            roll_a <= '0';
            roll_b <= '0';

            wba_valid_streams <= (others => '0');
            wbb_valid_streams <= (others => '0');

            wba_packages <= (others => '0');
            wbb_packages <= (others => '0');
            
            wba_ack_s_2 <= '0';
            wbb_ack_s_2 <= '0';
            wba_ack_s_3 <= '0';
            wbb_ack_s_3 <= '0';

		elsif (clk_i'event and clk_i = '1') then
            
			if (wba_stb_s_3 = '1' and wba_cyc_s_3 = '1') then
				wba_ack_s <= '1';
                wba_adr_latch <= wba_adr_s_3;
                
			else
				wba_ack_s <= '0';
			end if;
			
			if (wbb_stb_s_3 = '1' and wbb_cyc_s_3 = '1') then
                wbb_ack_s <= '1';
                wbb_adr_latch <= wbb_adr_s_3;
            else
                wbb_ack_s <= '0';
            end if;
            
            wba_ack_s_2 <= wba_ack_s;
            wbb_ack_s_2 <= wbb_ack_s;
            wba_ack_s_3 <= wba_ack_s_2;
            wbb_ack_s_3 <= wbb_ack_s_2;

            -- Only need to cover to cases for this style of operation
            if (wbb_ack_s = '1' and wbb_we_s_3 ='1' and wba_ack_s = '0' and not (ack_cnt = c_ALL_ONES)) then
                -- writing and not reading
                ack_cnt <= ack_cnt + 1;
            elsif (wba_ack_s = '1' and wbb_ack_s = '0' and wbb_we_s_3 = '0' and not (ack_cnt = c_ALL_ZEROS)) then
                -- not writing and reading
                ack_cnt <= ack_cnt - 1;
            end if;

            if (ack_cnt > bram_almost_full_i) then
                bram_busy <= '1';
            elsif (ack_cnt < bram_almost_empty_i) then
                bram_busy <= '0';
            end if;


            -- Rolling handling --
            -- Both rolling over: keep it the same
            if((wba_adr_s = x"FFFFFFFF") and (wbb_adr_s = x"FFFFFFFF")) then
                roll_a <= '0';
                roll_b <= '0';
            -- One rolling over: 
            -- B | 1 -> 0, 0 -> 0
            -- A | 0 -> 0, 0 -> 1
            --
            -- A roll
            elsif(wba_adr_s_3 = x"FFFFFFFF") then 
                roll_a <= not roll_b;
                roll_b <= '0';
            -- B roll
            elsif(wbb_adr_s_3 = x"FFFFFFFF") then
                roll_b <= not roll_a;
                roll_a <= '0';
            end if;
            
            adr_diff_a <= "000" & (unsigned(wba_adr_latch(28 downto 0)) - unsigned(wbb_adr_latch(28 downto 0)));
            adr_diff_b <= "000" & (unsigned(wbb_adr_latch(28 downto 0)) - unsigned(wba_adr_latch(28 downto 0)));
 
            -- B is ahead of A, AND B - A < big_number (A can rollover before B), OR A - B > big_number (B rolled over before A)
            if(((wbb_adr_latch >= wba_adr_latch) and (adr_diff_b < MAX_ADDR_DIFF_C)) or (adr_diff_a > MAX_ADDR_DIFF_C)) then
                adr_diff <= adr_diff_b;
            else -- A ahead of B
                adr_diff <= (others => '0');
            end if;

            if (adr_diff >= bram_almost_full_i) then
                bram_busy_adr <= '1';
            elsif (adr_diff <= bram_almost_empty_i) then
                bram_busy_adr <= '0';
            else
                bram_busy_adr <= bram_busy_adr;
            end if;

            -- A streams out
            if(wba_ack_s_3 = '1' and (wba_dat_o_s(31) = '1')) then
                wba_valid_streams <= wba_valid_streams + 1;
            end if;
            
            -- B streams in
            if(wbb_stb_s_3 = '1' and (wbb_dat_i_s_3(31) = '1')) then
                wbb_valid_streams <= wbb_valid_streams + 1;
            end if;

            -- A packages
            if(wba_ack_s_3 = '1') then
                wba_packages <= wba_packages + 1;
            end if;
            -- B packages
            if(wbb_stb_s_3 = '1') then
                wbb_packages <= wbb_packages + 1;
            end if;   
            
		end if;
	end process bram;
    
    bram_streams_in  <= wbb_valid_streams;
    bram_packages_in <= wbb_packages;
    bram_streams_out <= wba_valid_streams;
    bram_packages_out <= wba_packages;

	process(clk_i)
    begin
       if (clk_i'event and clk_i = '1') then
           selecta_s <= wba_adr_s_3(BLOCK_ADDR_WIDTH_C+BLOCK_COL_EXP_C-1 downto BLOCK_ADDR_WIDTH_C);
           selectb_s <= wbb_adr_s_3(BLOCK_ADDR_WIDTH_C+BLOCK_COL_EXP_C-1 downto BLOCK_ADDR_WIDTH_C);

           selecta_s_2 <= selecta_s;
           selectb_s_2 <= selectb_s;
       end if;
    end process;

    WEA_S <= (others => '1') when wba_we_s_3 = '1' else
             (others => '0');
    WEB_S <= (others => '1') when wbb_we_s_3 = '1' else
             (others => '0');    

	output_delay_p:process(clk_i)
    begin   
    	if (rst_i ='1') then
            wba_dat_o_s <= (others => '0');
            wbb_dat_o_s <= (others => '0');
            wba_ack_o <= '0';
            wbb_ack_o <= '0';
        elsif (clk_i'event and clk_i = '1') then          
            wba_dat_o_s <= wba_dat_a(TO_INTEGER(unsigned(selecta_s_2)));
            wbb_dat_o_s <= wbb_dat_a(TO_INTEGER(unsigned(selectb_s_2)));
            wba_ack_o <= wba_ack_s_2;
            wbb_ack_o <= wbb_ack_s_2;
            wba_dat_o_s_2 <= wba_dat_o_s;
            wbb_dat_o_s_2 <= wbb_dat_o_s;
        end if;
   end process;
   
   wba_dat_o <= wba_dat_o_s;
   wbb_dat_o <= wbb_dat_o_s;
   
   
   
   -- BRAM_TDP_MACRO: True Dual Port RAM
   --                 Kintex-7
   -- Xilinx HDL Language Template, version 2016.2

   -- Note -  This Unimacro model assumes the port directions to be "downto". 
   --         Simulation of this model with "to" in the port directions could lead to erroneous results.

   --------------------------------------------------------------------------
   -- DATA_WIDTH_A/B | BRAM_SIZE | RAM Depth | ADDRA/B Width | WEA/B Width --
   -- ===============|===========|===========|===============|=============--
   --     19-36      |  "36Kb"   |    1024   |    10-bit     |    4-bit    --
   --     10-18      |  "36Kb"   |    2048   |    11-bit     |    2-bit    --
   --     10-18      |  "18Kb"   |    1024   |    10-bit     |    2-bit    --
   --      5-9       |  "36Kb"   |    4096   |    12-bit     |    1-bit    --
   --      5-9       |  "18Kb"   |    2048   |    11-bit     |    1-bit    --
   --      3-4       |  "36Kb"   |    8192   |    13-bit     |    1-bit    --
   --      3-4       |  "18Kb"   |    4096   |    12-bit     |    1-bit    --
   --        2       |  "36Kb"   |   16384   |    14-bit     |    1-bit    --
   --        2       |  "18Kb"   |    8192   |    13-bit     |    1-bit    --
   --        1       |  "36Kb"   |   32768   |    15-bit     |    1-bit    --
   --        1       |  "18Kb"   |   16384   |    14-bit     |    1-bit    --
   --------------------------------------------------------------------------
   gen_bram_col:for j in 0 to BLOCK_COL_C-1 generate
   
   	wba_cyc_v_s(j)       <= '1' when wba_adr_s_3(BLOCK_ADDR_WIDTH_C+BLOCK_COL_EXP_C-1 downto BLOCK_ADDR_WIDTH_C) = std_logic_vector(to_unsigned(j,BLOCK_COL_EXP_C))  
                                    else '0';
   
   	wbb_cyc_v_s(j)       <= '1' when wbb_adr_s_3(BLOCK_ADDR_WIDTH_C+BLOCK_COL_EXP_C-1 downto BLOCK_ADDR_WIDTH_C) = std_logic_vector(to_unsigned(j,BLOCK_COL_EXP_C))  
                                    else '0';   
   
   gen_bram_row:for i in 0 to BLOCK_ROW_C-1 generate
   
   
   BRAM_TDP_MACRO_inst : BRAM_TDP_MACRO
   generic map (
      BRAM_SIZE => "18Kb", -- Target BRAM, "18Kb" or "36Kb" 
      DEVICE => "7SERIES", -- Target Device: "VIRTEX5", "VIRTEX6", "7SERIES", "SPARTAN6" 
      DOA_REG => 1, -- Optional port A output register (0 or 1)
      DOB_REG => 1, -- Optional port B output register (0 or 1)
      INIT_A => X"000000000", -- Initial values on A output port
      INIT_B => X"000000000", -- Initial values on B output port
      INIT_FILE => "NONE",
      READ_WIDTH_A => BLOCK_DATA_WIDTH_C,   -- Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
      READ_WIDTH_B => BLOCK_DATA_WIDTH_C,   -- Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
      SIM_COLLISION_CHECK => "ALL", -- Collision check enable "ALL", "WARNING_ONLY", 
                                    -- "GENERATE_X_ONLY" or "NONE" 
      SRVAL_A => X"000000000",   -- Set/Reset value for A port output
      SRVAL_B => X"000000000",   -- Set/Reset value for B port output
      WRITE_MODE_A => "WRITE_FIRST", -- "WRITE_FIRST", "READ_FIRST" or "NO_CHANGE" 
      WRITE_MODE_B => "WRITE_FIRST", -- "WRITE_FIRST", "READ_FIRST" or "NO_CHANGE" 
      WRITE_WIDTH_A => BLOCK_DATA_WIDTH_C, -- Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
      WRITE_WIDTH_B => BLOCK_DATA_WIDTH_C, -- Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
      -- The following INIT_xx declarations specify the initial contents of the RAM
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      
      -- The next set of INIT_xx are valid when configured as 36Kb
      INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
      
      -- The next set of INITP_xx are for the parity bits
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      
      -- The next set of INIT_xx are valid when configured as 36Kb
      INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
   port map (
      DOA => wba_dat_a(j)(BLOCK_DATA_WIDTH_C-1+BLOCK_DATA_WIDTH_C*i downto 0+BLOCK_DATA_WIDTH_C*i),       -- Output port-A data, width defined by READ_WIDTH_A parameter
      DOB => wbb_dat_a(j)(BLOCK_DATA_WIDTH_C-1+BLOCK_DATA_WIDTH_C*i downto 0+BLOCK_DATA_WIDTH_C*i),       -- Output port-B data, width defined by READ_WIDTH_B parameter
      ADDRA => wba_adr_s_3(BLOCK_ADDR_WIDTH_C-1 downto 0),   -- Input port-A address, width defined by Port A depth
      ADDRB => wbb_adr_s_3(BLOCK_ADDR_WIDTH_C-1 downto 0),   -- Input port-B address, width defined by Port B depth
      CLKA => clk_i,     -- 1-bit input port-A clock
      CLKB => clk_i,     -- 1-bit input port-B clock
      DIA => wba_dat_i_s_3(BLOCK_DATA_WIDTH_C-1+BLOCK_DATA_WIDTH_C*i downto 0+BLOCK_DATA_WIDTH_C*i),       -- Input port-A data, width defined by WRITE_WIDTH_A parameter
      DIB => wbb_dat_i_s_3(BLOCK_DATA_WIDTH_C-1+BLOCK_DATA_WIDTH_C*i downto 0+BLOCK_DATA_WIDTH_C*i),       -- Input port-B data, width defined by WRITE_WIDTH_B parameter
      ENA => wba_cyc_v_s(j),       -- 1-bit input port-A enable
      ENB => wbb_cyc_v_s(j),       -- 1-bit input port-B enable
      REGCEA => '1', -- 1-bit input port-A output register enable
      REGCEB => '1', -- 1-bit input port-B output register enable
      RSTA => rst_i,     -- 1-bit input port-A reset
      RSTB => rst_i,     -- 1-bit input port-B reset
      WEA => WEA_S,       -- Input port-A write enable, width defined by Port A depth
      WEB => WEB_S        -- Input port-B write enable, width defined by Port B depth
   );
   
   end generate;
   end generate;
-- End of BRAM_TDP_MACRO_inst instantiation



end Behavioral;
