-- ####################################
-- # Project: Yarr
-- # Author: Timon Heim
-- # E-Mail: timon.heim at cern.ch
-- # Comments: RX core
-- # Outputs are synchronous to wb_clk_i
-- ####################################
-- # Adress Map:
-- # Adr[3:0]:
-- #     0x0 : RX Enable Mask
-- #     0x1 : RX Sync Status
-- #     0x2 : RX Polarity
-- #	 0x3 : RX Active Lanes
-- #     0x4 : RX select lane
-- #     0x5 : Set lane delay
-- #     0x6 : Set auto/manual delay adjustment
-- #     0x7 : Write lane delay to selected lane
-- #     0x8 : Error counter stop value
-- #     0x9 : Error counter mode
-- #     0xA : Error counter target
-- #     0xB : Error counter reset/read value
-- #     0xC : Total valid streams counter (RO)
-- #     0xD : Total package counter (RO)

library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.VComponents.all;

Library xpm;
use xpm.vcomponents.all;

entity wb_rx_core is
	generic (
		g_NUM_RX : integer range 1 to 32 := 1;
        g_TYPE : string := "FEI4";
        g_NUM_LANES : integer range 1 to 4 := 1
	);
	port (
		-- Sys connect
		wb_clk_i	: in  std_logic;
		rst_n_i		: in  std_logic;
		
		-- Wishbone slave interface
		wb_adr_i	: in  std_logic_vector(31 downto 0);
		wb_dat_i	: in  std_logic_vector(31 downto 0);
		wb_dat_o	: out std_logic_vector(31 downto 0);
		wb_cyc_i	: in  std_logic;
		wb_stb_i	: in  std_logic;
		wb_we_i		: in  std_logic;
		wb_ack_o	: out std_logic;
		wb_stall_o	: out std_logic;
		
		-- RX IN
		rx_clk_i	: in  std_logic;
		rx_serdes_clk_i : in std_logic;
		rx_data_i_p	: in std_logic_vector((g_NUM_RX*g_NUM_LANES)-1 downto 0);
		rx_data_i_n	: in std_logic_vector((g_NUM_RX*g_NUM_LANES)-1 downto 0);
        trig_tag_i : in std_logic_vector(31 downto 0);
		
		-- RX OUT (sync to sys_clk)
		rx_valid_o : out std_logic;
		rx_data_o : out std_logic_vector(63 downto 0);
		busy_o : out std_logic;
		arbiter_cyc_end_o : out std_logic;
		
		debug_o : out std_logic_vector(31 downto 0)

	


	);
end wb_rx_core;

architecture behavioral of wb_rx_core is
	function log2_ceil(val : integer) return natural is
		 variable result : natural;
	begin
		 for i in 0 to g_NUM_RX-1 loop
			 if (val <= (2 ** i)) then
				 result := i;
				 exit;
			 end if;
		 end loop;
		 return result;
	end function;

	constant c_ALL_ZEROS : std_logic_vector(g_NUM_RX-1 downto 0) := (others => '0');
	
	component frr_arbiter
		generic (
			g_CHANNELS : integer := g_NUM_RX
		);
		port (
			-- sys connect
			clk_i : in std_logic;
			rst_i : in std_logic;
			-- requests
			req_i : in std_logic_vector(g_CHANNELS-1 downto 0);
            en_i : in std_logic_vector(g_CHANNELS-1 downto 0);
            -- status out
            eoc_o : out std_logic;
			-- grants
			gnt_o : out std_logic_vector(g_CHANNELS-1 downto 0)
		);
	end component frr_arbiter;

	component fei4_rx_channel
		port (
			-- Sys connect
			rst_n_i : in std_logic;
			clk_160_i : in std_logic;
			clk_640_i : in std_logic;
			enable_i : in std_logic;
			-- Input
			rx_data_i : in std_logic;
            trig_tag_i : in std_logic_vector(31 downto 0);
			-- Output
			rx_data_o : out std_logic_vector(25 downto 0);
			rx_valid_o : out std_logic;
			rx_stat_o : out std_logic_vector(7 downto 0);
			rx_data_raw_o : out std_logic_vector(7 downto 0)
		);
	end component;

    component aurora_rx_channel
        generic (
            g_NUM_LANES : integer range 1 to 4 := g_NUM_LANES
        );
        port (
            rst_n_i : in std_logic;
            clk_rx_i : in std_logic; -- Fabric clock (serdes/8)
            clk_serdes_i : in std_logic; -- IO clock
			sys_clk_i : in std_logic; -- wb_clk

			-- Input
            enable_i : in std_logic;
            rx_data_i_p : in std_logic_vector(g_NUM_LANES-1 downto 0);
            rx_data_i_n : in std_logic_vector(g_NUM_LANES-1 downto 0);
            rx_polarity_i : in std_logic_vector(g_NUM_LANES-1 downto 0);
            trig_tag_i : in std_logic_vector(63 downto 0);
            rx_active_lanes_i : in std_logic_vector(g_NUM_LANES-1 downto 0);
            rx_lane_delay_i : in std_logic_vector((g_NUM_LANES*5)-1 downto 0);
            rx_manual_delay_i : in std_logic_vector(g_NUM_LANES-1 downto 0);

			-- Output
            rx_data_o : out std_logic_vector(63 downto 0);
            rx_valid_o : out std_logic;
            rx_stat_o : out std_logic_vector(7 downto 0);
			rx_lane_delay_o : out std_logic_vector((g_NUM_LANES*5)-1 downto 0);
			
			-- Errors
            data_errors : out std_logic_vector((g_NUM_LANES*32)-1 downto 0);
            data_error_stop : in std_logic_vector(31 downto 0);
            data_error_stop_req : in std_logic;
            data_error_mode : in std_logic_vector(1 downto 0);
            data_error_reset : in std_logic
			
        );
    end component aurora_rx_channel;
	
	COMPONENT rx_channel_fifo
		PORT (
			rst : IN STD_LOGIC;
			wr_clk : IN STD_LOGIC;
			rd_clk : IN STD_LOGIC;
			din : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
			wr_en : IN STD_LOGIC;
			rd_en : IN STD_LOGIC;
			dout : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
			full : OUT STD_LOGIC;
			empty : OUT STD_LOGIC
		);
	END COMPONENT;
	
    COMPONENT ila_rx_dma_wb
    PORT (
        clk : IN STD_LOGIC;
        probe0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0); 
        probe1 : IN STD_LOGIC_VECTOR(63 DOWNTO 0); 
        probe2 : IN STD_LOGIC_VECTOR(63 DOWNTO 0); 
        probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe7 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe8 : IN STD_LOGIC_VECTOR(31 DOWNTO 0); 
        probe9 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe10 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe11 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
    );
    END COMPONENT  ;
	
    type rx_data_array is array (g_NUM_RX-1 downto 0) of std_logic_vector(63 downto 0);
	type rx_data_fifo_array is array (g_NUM_RX-1 downto 0) of std_logic_vector(63 downto 0);
	type rx_stat_array is array (g_NUM_RX-1 downto 0) of std_logic_vector(7 downto 0);
    signal rx_data_i : std_logic_vector((g_NUM_RX*g_NUM_LANES)-1 downto 0);
    signal rx_polarity : std_logic_vector((g_NUM_RX*g_NUM_LANES)-1 downto 0);
    signal rx_polarity_t : std_logic_vector((g_NUM_RX*g_NUM_LANES)-1 downto 0);
    signal rx_data : rx_data_array;
	signal rx_valid : std_logic_vector(g_NUM_RX-1 downto 0);
	signal rx_stat : rx_stat_array;
	signal rx_data_raw : rx_stat_array;
	
	signal rx_fifo_dout :rx_data_fifo_array;
	signal rx_fifo_dout_t :rx_data_fifo_array;
	signal rx_fifo_din : rx_data_fifo_array;
	signal rx_fifo_full : std_logic_vector(g_NUM_RX-1 downto 0);
	signal rx_fifo_empty : std_logic_vector(g_NUM_RX-1 downto 0);
	signal rx_fifo_empty_t : std_logic_vector(g_NUM_RX-1 downto 0);
	signal rx_fifo_rden : std_logic_vector(g_NUM_RX-1 downto 0);
	signal rx_fifo_rden_t : std_logic_vector(g_NUM_RX-1 downto 0);
	signal rx_fifo_wren : std_logic_vector(g_NUM_RX-1 downto 0);

	signal rx_enable : std_logic_vector(31 downto 0);
	signal rx_enable_d : std_logic_vector(31 downto 0);
	signal rx_enable_dd : std_logic_vector(31 downto 0);
    signal rx_status : std_logic_vector(31 downto 0);
    signal rx_status_s : std_logic_vector(31 downto 0);

    signal rx_active_lanes : std_logic_vector(3 downto 0); -- Max of 4-lanes
    signal rx_active_lanes_t : std_logic_vector(3 downto 0);

    signal rx_lane_sel : std_logic_vector(31 downto 0);
	signal rx_manual_delay : std_logic_vector((g_NUM_RX*g_NUM_LANES)-1 downto 0);
    signal rx_lane_delay : std_logic_vector((g_NUM_RX*g_NUM_LANES*5)-1 downto 0);
	signal rx_lane_delay_o : std_logic_vector((g_NUM_RX*g_NUM_LANES*5)-1 downto 0);
	signal rx_lane_delay_os : std_logic_vector((g_NUM_RX*g_NUM_LANES*5)-1 downto 0);
    signal rx_lane_delay_s : std_logic_vector((g_NUM_RX*g_NUM_LANES*5)-1 downto 0);
    signal rx_lane_delay_s_t : std_logic_vector((g_NUM_RX*g_NUM_LANES*5)-1 downto 0);
    
    signal rx_lane_delay_idx_0 : integer;
    signal rx_lane_delay_idx_1 : integer;
    signal rx_lane_delay_idx_2 : integer;
    signal rx_lane_delay_idx_3 : integer;
    signal rx_lane_delay_idx_4 : integer;

	signal channel : integer range 0 to g_NUM_RX-1;
	
	signal arbiter_cyc_end_t : std_logic;

	signal debug : std_logic_vector(31 downto 0);
	     
    signal data_errors : std_logic_vector((g_NUM_RX*g_NUM_LANES*32)-1 downto 0);
    signal data_error_stop : std_logic_vector(31 downto 0);
    signal data_error_stop_160 : std_logic_vector(31 downto 0);
    signal data_error_mode : std_logic_vector(1 downto 0);
    signal data_error_target : std_logic_vector(31 downto 0);
    signal data_error_reset : std_logic;

	signal rx_valid_streams_cnt : unsigned(31 downto 0);
	signal rx_total_package_cnt : unsigned(31 downto 0);
	signal rx_empty_word_cnt : unsigned(31 downto 0);
    signal rx_valid_int : std_logic;

    signal data_error_stop_send : std_logic;
    signal data_error_stop_rcv : std_logic;
    signal data_error_stop_req : std_logic;
begin
	debug_o <= debug;
	
	debug(7 downto 0) <= rx_stat(0);
	debug(15 downto 8) <= rx_data_raw(0);
	debug(16) <= rx_valid(0);

--    wb_core_debug : ila_rx_dma_wb
--    PORT MAP (
--      clk => wb_clk_i,
--      probe0 => std_logic_vector(rx_valid_streams_cnt), 
--      probe1 => rx_fifo_dout(channel), 
--      probe2 => (others => '0'), 
--      probe3(0) => rx_fifo_empty(0),
--      probe4(0) => rx_enable_d(0),
--      probe5(0) => rx_valid_int, 
--      probe6(0) => rx_fifo_rden_t(0),
--      probe7(0) => '0',
--      probe8(g_NUM_RX - 1 downto 0) => std_logic_vector( to_unsigned(channel, g_NUM_RX)),
-- 	 probe8(31 downto g_NUM_RX) => (others => '0'),
--      probe9(0) => '0',
--      probe10(0) => '0',
--      probe11(0) => '0'
--    );


    wb_proc: process (wb_clk_i, rst_n_i)
	begin
		if (rst_n_i = '0') then
			wb_dat_o <= (others => '0');
			wb_ack_o <= '0';
			rx_enable <= (others => '0');
			wb_stall_o <= '0';
			rx_enable_d <= (others => '0');
            rx_polarity <= (others => '0');
            rx_active_lanes <= (others => '1');
            rx_status <= (others => '0');
		elsif rising_edge(wb_clk_i) then
			wb_ack_o <= '0';
			rx_enable_d <= rx_enable;
            rx_status <= rx_status_s;
	    	rx_lane_delay <= rx_lane_delay_s;
            rx_lane_delay_os <= rx_lane_delay_o;
            if (data_error_stop_rcv = '1') then
                data_error_stop_send <= '0';
            end if;
			if (wb_cyc_i = '1' and wb_stb_i = '1') then
				if (wb_we_i = '1') then
					if (wb_adr_i(3 downto 0) = x"0") then -- Set enable mask
						wb_ack_o <= '1';
						rx_enable <= wb_dat_i;
					elsif (wb_adr_i(3 downto 0) = x"2") then -- Set RX polarity
						wb_ack_o <= '1';
						rx_polarity <= wb_dat_i((g_NUM_RX*g_NUM_LANES)-1 downto 0);
					elsif (wb_adr_i(3 downto 0) = x"3") then -- Set Num of active lanes
						wb_ack_o <= '1';
						rx_active_lanes <= wb_dat_i(3 downto 0);
                   	elsif (wb_adr_i(3 downto 0) = x"4") then -- Select lane
						wb_ack_o <= '1';
						rx_lane_sel <= wb_dat_i;
						rx_lane_delay_idx_0 <= TO_INTEGER(unsigned(wb_dat_i))*5;
						rx_lane_delay_idx_1 <= (TO_INTEGER(unsigned(wb_dat_i))*5)+1;
						rx_lane_delay_idx_2 <= (TO_INTEGER(unsigned(wb_dat_i))*5)+2;
						rx_lane_delay_idx_3 <= (TO_INTEGER(unsigned(wb_dat_i))*5)+3;
						rx_lane_delay_idx_4 <= (TO_INTEGER(unsigned(wb_dat_i))*5)+4;
                    elsif (wb_adr_i(3 downto 0) = x"5") then -- Write lane delay to selected lane
						wb_ack_o <= '1';
						rx_lane_delay_s(rx_lane_delay_idx_4) <= wb_dat_i(4);
						rx_lane_delay_s(rx_lane_delay_idx_3) <= wb_dat_i(3);
						rx_lane_delay_s(rx_lane_delay_idx_2) <= wb_dat_i(2);
						rx_lane_delay_s(rx_lane_delay_idx_1) <= wb_dat_i(1);
						rx_lane_delay_s(rx_lane_delay_idx_0) <= wb_dat_i(0);
					elsif (wb_adr_i(3 downto 0) = x"6") then -- Set enable auto or manual delay adjustement
						wb_ack_o <= '1';
						rx_manual_delay <= wb_dat_i((g_NUM_RX*g_NUM_LANES)-1 downto 0);
					elsif (wb_adr_i(3 downto 0) = x"7") then -- Write lane delay to selected lane RO
						wb_ack_o <= '1';
				    elsif (wb_adr_i(3 downto 0) = x"8") then -- Set error counter stop value
                        wb_ack_o <= '1';
                        data_error_stop <= wb_dat_i(31 downto 0);
                        data_error_stop_send <= '1';
                    elsif (wb_adr_i(3 downto 0) = x"9") then -- Set error counter mode
                        wb_ack_o <= '1';
                        data_error_mode <= wb_dat_i(1 downto 0);
                    elsif (wb_adr_i(3 downto 0) = x"a") then -- Set error counter target
                        wb_ack_o <= '1';
                        data_error_target <= wb_dat_i;
                    elsif (wb_adr_i(3 downto 0) = x"b") then -- Set error counter reset
                        wb_ack_o <= '1';
                        data_error_reset <= wb_dat_i(0);
					elsif (wb_adr_i(3 downto 0) = x"c") then -- Set total counter (do nothing)
						wb_ack_o <= '1';
					else
						wb_ack_o <= '1';
					end if;
				else
					if (wb_adr_i(3 downto 0) = x"0") then -- Read enable mask
						wb_dat_o <= rx_enable;
						wb_ack_o <= '1';
                    elsif (wb_adr_i(3 downto 0) = x"1") then -- Link status
                        wb_dat_o <= rx_status;
                       	wb_ack_o <= '1';
                    elsif (wb_adr_i(3 downto 0) = x"2") then -- RX polarity
                        wb_dat_o <= (others => '0');
                        wb_dat_o((g_NUM_RX*g_NUM_LANES)-1 downto 0) <= rx_polarity;
                        wb_ack_o <= '1';
                    elsif (wb_adr_i(3 downto 0) = x"3") then -- Num of active lanes
                        wb_dat_o <= (others => '0');
                       	wb_dat_o(3 downto 0) <= rx_active_lanes;
                        wb_ack_o <= '1';
                    elsif (wb_adr_i(3 downto 0) = x"4") then -- Select lane
						wb_ack_o <= '1';
						wb_dat_o <= rx_lane_sel;
                    elsif (wb_adr_i(3 downto 0) = x"5") then -- Read lane delay 
						wb_ack_o <= '1';
						wb_dat_o <= (others => '0');
						wb_dat_o(4) <= rx_lane_delay_s(rx_lane_delay_idx_4);
						wb_dat_o(3) <= rx_lane_delay_s(rx_lane_delay_idx_3);
						wb_dat_o(2) <= rx_lane_delay_s(rx_lane_delay_idx_2);
						wb_dat_o(1) <= rx_lane_delay_s(rx_lane_delay_idx_1);
						wb_dat_o(0) <= rx_lane_delay_s(rx_lane_delay_idx_0);
					elsif (wb_adr_i(3 downto 0) = x"6") then -- Read enable auto or manual delay adjustement
						wb_ack_o <= '1';
						wb_dat_o <= (others => '0');
						wb_dat_o((g_NUM_RX*g_NUM_LANES)-1 downto 0) <= rx_manual_delay;
					elsif (wb_adr_i(3 downto 0) = x"7") then -- Read lane delay 
						wb_ack_o <= '1';
						wb_dat_o <= (others => '0');
						wb_dat_o(4) <= rx_lane_delay_os(rx_lane_delay_idx_4);
						wb_dat_o(3) <= rx_lane_delay_os(rx_lane_delay_idx_3);
						wb_dat_o(2) <= rx_lane_delay_os(rx_lane_delay_idx_2);
						wb_dat_o(1) <= rx_lane_delay_os(rx_lane_delay_idx_1);
						wb_dat_o(0) <= rx_lane_delay_os(rx_lane_delay_idx_0);
					 elsif (wb_adr_i(3 downto 0) = x"b") then -- Num of errors on rx target
                        wb_dat_o <= (others => '0');
                        wb_dat_o <= data_errors(((TO_INTEGER(unsigned(data_error_target))+1)*32)-1 downto TO_INTEGER(unsigned(data_error_target))*32);
                        wb_ack_o <= '1';
					elsif (wb_adr_i(3 downto 0) = x"c") then -- Read RX stream count
						wb_dat_o <= std_logic_vector(rx_valid_streams_cnt);
						wb_ack_o <= '1';
					elsif (wb_adr_i(3 downto 0) = x"d") then -- Read total RX package count
						wb_dat_o <= std_logic_vector(rx_total_package_cnt);
						wb_ack_o <= '1';
					elsif (wb_adr_i(3 downto 0) = x"e") then -- Read total empty word count
						wb_dat_o <= std_logic_vector(rx_empty_word_cnt);
						wb_ack_o <= '1';
					else
						wb_dat_o <= x"DEADBEEF";
						wb_ack_o <= '1';
					end if;
				end if;
			end if;
		end if;
	end process wb_proc;
	
	-- Arbiter
	cmp_frr_arbiter : frr_arbiter port map (
		clk_i => wb_clk_i,
		rst_i => not rst_n_i,
		req_i => not rx_fifo_empty_t,
        en_i => rx_enable_dd(g_NUM_RX-1 downto 0), 
        eoc_o => arbiter_cyc_end_t,
		gnt_o => rx_fifo_rden_t
	);
	
	-- CDC handshake
	xpm_cdc_handshake_data_error_stop : xpm_cdc_handshake
    generic map (
       DEST_EXT_HSK => 0,   -- DECIMAL; 0=internal handshake, 1=external handshake
       DEST_SYNC_FF => 2,   -- DECIMAL; range: 2-10
       INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
       SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
       SRC_SYNC_FF => 2,    -- DECIMAL; range: 2-10
       WIDTH => 32           -- DECIMAL; range: 1-1024
    )
    port map (
       dest_out => data_error_stop_160, -- WIDTH-bit output: Input bus (src_in) synchronized to destination clock domain.
                             -- This output is registered.
    
       dest_req => data_error_stop_req, -- 1-bit output: Assertion of this signal indicates that new dest_out data has been
                             -- received and is ready to be used or captured by the destination logic. When
                             -- DEST_EXT_HSK = 1, this signal will deassert once the source handshake
                             -- acknowledges that the destination clock domain has received the transferred
                             -- data. When DEST_EXT_HSK = 0, this signal asserts for one clock period when
                             -- dest_out bus is valid. This output is registered.
    
       src_rcv => data_error_stop_rcv,   -- 1-bit output: Acknowledgement from destination logic that src_in has been
                             -- received. This signal will be deasserted once destination handshake has fully
                             -- completed, thus completing a full data transfer. This output is registered.
    
       dest_ack => '0', -- 1-bit input: optional; required when DEST_EXT_HSK = 1
       dest_clk => rx_clk_i, -- 1-bit input: Destination clock.
       src_clk => wb_clk_i,   -- 1-bit input: Source clock.
       src_in => data_error_stop,     -- WIDTH-bit input: Input bus that will be synchronized to the destination clock
                             -- domain.
    
       src_send => data_error_stop_send  -- 1-bit input: Assertion of this signal allows the src_in bus to be synchronized
                             -- to the destination clock domain. This signal should only be asserted when
                             -- src_rcv is deasserted, indicating that the previous data transfer is complete.
                             -- This signal should only be deasserted once src_rcv is asserted, acknowledging
                             -- that the src_in has been received by the destination logic.
    
    );
	
	--rx_valid_o <= '0' when (unsigned(rx_fifo_rden) = 0 or ((rx_fifo_rden and rx_fifo_empty) = rx_fifo_rden)) else '1';
	--rx_data_o <= x"DEADBEEF" when (unsigned(rx_fifo_rden) = 0) else rx_fifo_dout(log2_ceil(to_integer(unsigned(rx_fifo_rden))));
	
	reg_proc : process(wb_clk_i, rst_n_i)
	begin
		if (rst_n_i = '0') then
			--rx_fifo_rden <= (others => '0');
			rx_valid_o <= '0';
			rx_valid_int <= '0';
			channel <= 0;
			rx_fifo_dout <= (others => (others => '0'));
			rx_fifo_rden <= (others => '0');
			rx_fifo_empty <= (others => '0');
			rx_valid_streams_cnt <= (others => '0');
			rx_total_package_cnt <= (others => '0');
			rx_empty_word_cnt <= (others => '0');
		elsif rising_edge(wb_clk_i) then
            rx_fifo_dout  <= rx_fifo_dout_t;
            rx_fifo_rden <= rx_fifo_rden_t;
            rx_fifo_empty <= rx_fifo_empty_t;
            channel <= log2_ceil(to_integer(unsigned(rx_fifo_rden_t)));
			if (unsigned(rx_fifo_rden) = 0) then -- no channel being filled right now
				rx_valid_o <= '0';
				rx_valid_int <= '0';
				arbiter_cyc_end_o <= arbiter_cyc_end_t;
				rx_data_o <= x"DEADBEEFDEADBEEF";
            elsif ((rx_fifo_rden and rx_fifo_empty) = rx_fifo_rden) then -- channel being read is empty, add empty word
				rx_valid_o <= '1';
				rx_valid_int <= '1';
				arbiter_cyc_end_o <= arbiter_cyc_end_t;
				rx_data_o <= x"FFFFDEADFFFFDEAD";
				rx_empty_word_cnt <= rx_empty_word_cnt + 1;
            else -- channel being read is not empty
				rx_valid_o <= '1';
				rx_valid_int <= '1';
				arbiter_cyc_end_o <= arbiter_cyc_end_t;
				rx_data_o <= rx_fifo_dout(channel);
				if(rx_fifo_dout(channel)(31) = '1') then
					rx_valid_streams_cnt <= rx_valid_streams_cnt + 1;
				end if;
				rx_total_package_cnt <= rx_total_package_cnt + 1;
			end if;
		end if;
	end process reg_proc;
    
    fei4_iobuf: if g_TYPE = "FEI4" generate
        rx_loop: for I in 0 to (g_NUM_RX*g_NUM_LANES)-1 generate
        begin
            rx_buf : IBUFDS
            generic map (
                DIFF_TERM => TRUE, -- Differential Termination 
                IBUF_LOW_PWR => FALSE, -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
                IOSTANDARD => "LVDS_25")
            port map (
                O => rx_data_i(I),  -- Buffer output
                I => rx_data_i_p(I),  -- Diff_p buffer input (connect directly to top-level port)
                IB => rx_data_i_n(I) -- Diff_n buffer input (connect directly to top-level port)
            );
        end generate;
    end generate fei4_iobuf;
    
    enable_sync: process (rx_clk_i, rst_n_i)
    begin
        if (rst_n_i = '0') then
            rx_enable_dd <= (others => '0');
            rx_polarity_t <= (others => '0');
            rx_active_lanes_t <= (others => '1');
        elsif rising_edge(rx_clk_i) then
            rx_enable_dd <= rx_enable_d;
            rx_polarity_t <= rx_polarity;
            rx_active_lanes_t <= rx_active_lanes;
        end if;
   end process enable_sync;
	
    -- Generate Rx Channels
	busy_o <= '0' when (rx_fifo_full = c_ALL_ZEROS) else '1';
	rx_channels: for I in 0 to g_NUM_RX-1 generate
	begin
        fei4_type: if g_TYPE = "FEI4" generate
            cmp_fei4_rx_channel: fei4_rx_channel PORT MAP(
                rst_n_i => rst_n_i,
                clk_160_i => rx_clk_i,
                clk_640_i => rx_serdes_clk_i,
                enable_i => rx_enable_dd(I),
                rx_data_i => rx_data_i(I),
                trig_tag_i => trig_tag_i,
                rx_data_o => rx_data(I)(25 downto 0),
                rx_valid_o => rx_valid(I),
                rx_stat_o => rx_stat(I),
                rx_data_raw_o => rx_data_raw(I)
            );
		    rx_fifo_din(I) <= x"03000000" & STD_LOGIC_VECTOR(TO_UNSIGNED(I,6)) & rx_data(I)(25 downto 0);
        end generate fei4_type;
        
        rd53_type: if g_TYPE = "RD53" generate
            cmp_aurora_rx_channel : aurora_rx_channel PORT MAP (
                rst_n_i => rst_n_i,
                clk_rx_i => rx_clk_i,
                clk_serdes_i => rx_serdes_clk_i,
				sys_clk_i => wb_clk_i,
                enable_i => rx_enable_dd(I),
                rx_data_i_p => rx_data_i_p((I+1)*g_NUM_LANES-1 downto (I*g_NUM_LANES)),
                rx_data_i_n => rx_data_i_n((I+1)*g_NUM_LANES-1 downto (I*g_NUM_LANES)),
                rx_polarity_i => rx_polarity_t((I+1)*g_NUM_LANES-1 downto (I*g_NUM_LANES)),
                rx_active_lanes_i => rx_active_lanes_t(g_NUM_LANES-1 downto 0),
                rx_lane_delay_i => rx_lane_delay(((I+1)*g_NUM_LANES*5)-1 downto (I*g_NUM_LANES*5)),
				rx_lane_delay_o => rx_lane_delay_o(((I+1)*g_NUM_LANES*5)-1 downto (I*g_NUM_LANES*5)),
				rx_manual_delay_i => rx_manual_delay((I+1)*g_NUM_LANES-1 downto (I*g_NUM_LANES)),
                trig_tag_i => x"00000000" & trig_tag_i,
                rx_data_o => rx_data(I),
                rx_valid_o => rx_valid(I),
                rx_stat_o => rx_stat(I),
                data_errors => data_errors(((I+1)*g_NUM_LANES*32)-1 downto (I*g_NUM_LANES*32)),
                data_error_mode => data_error_mode,
                data_error_stop => data_error_stop_160,
                data_error_stop_req => data_error_stop_req,
                data_error_reset => data_error_reset
            );
            rx_status_s(((I+1)*g_NUM_LANES)-1 downto (I*g_NUM_LANES)) <= rx_stat(I)(3+g_NUM_LANES downto 4);
		    rx_fifo_din(I) <= rx_data(I);
        end generate rd53_type;
		
		rx_fifo_wren(I) <= rx_valid(I) and rx_enable_dd(I);
		cmp_rx_channel_fifo : rx_channel_fifo PORT MAP (
			rst => not rst_n_i,
			wr_clk => rx_clk_i,
			rd_clk => wb_clk_i,
			din => rx_fifo_din(I),
			wr_en => rx_fifo_wren(I),
			rd_en => rx_fifo_rden_t(I),
			dout => rx_fifo_dout_t(I),
			full => rx_fifo_full(I),
			empty => rx_fifo_empty_t(I)
		);
	end generate;
end behavioral;

