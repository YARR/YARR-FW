----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/16/2022 10:11:42 AM
-- Design Name: 
-- Module Name: error_counter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity error_counter is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           data : in STD_LOGIC_VECTOR (65 downto 0);
           counter_stop : in STD_LOGIC_VECTOR(31 downto 0);
           counter_stop_req : in STD_LOGIC;
           mode : in STD_LOGIC_VECTOR(1 downto 0);
           valid : in STD_LOGIC;
           errors : out STD_LOGIC_VECTOR (31 downto 0));
end error_counter;

architecture Behavioral of error_counter is
    signal counter : std_logic_vector(30 downto 0);
    signal notValid : std_logic_vector(30 downto 0);
begin
    process(clk) begin
        if clk'event and clk = '1' then
            if reset = '1' then
                counter <= (others => '0');
                notValid <= (others => '0');
            else
                if valid = '1' and ((not (counter = counter_stop(30 downto 0))) or (counter_stop(31) = '1')) then
                    counter <= std_logic_vector(unsigned(counter) + 1);
                    case mode is
                        when "00" => 
                            if (not (data(65 downto 56) = ("10" & x"78"))) then
                                notValid <= std_logic_vector(unsigned(notValid) + 1);
                            end if;
                        when "01" => 
                            if (data(65 downto 64) = "01") then
                                notValid <= std_logic_vector(unsigned(notValid) + 1);
                            end if;
                        when others =>
                            notValid <= (others => '0');
                    end case;
                end if;
            end if;
        end if;
    end process;

    errors(30 downto 0) <= notValid;
    errors(31) <= '1' when (counter = counter_stop(30 downto 0)) else '0';
end Behavioral;
