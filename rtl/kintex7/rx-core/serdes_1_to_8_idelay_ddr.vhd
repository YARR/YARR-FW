-- ####################################
-- # Project: Yarr
-- # Author: Timon Heim
-- # E-Mail: timon.heim@cern.ch
-- # Comments: Desearailizer and manual phase
--             adjustment
-- ####################################

library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim ;
use unisim.vcomponents.all ;

entity serdes_1_to_8_idelay_ddr is 
    port (
        -- Sys connect
        rst_n_i : in std_logic;
        sys_clk_i : in std_logic;
        delay_value_i : in std_logic_vector(4 downto 0);
        delay_value_o: out std_logic_vector(4 downto 0);

        -- Input
        clk_iserdes_ddr_i : in std_logic;
        rx_data_i : in std_logic;

        -- Output
        clk_rx_i : in std_logic;
        rx_data_o : out std_logic_vector(7 downto 0)
    );
end serdes_1_to_8_idelay_ddr;

architecture behavioral of serdes_1_to_8_idelay_ddr is

    attribute IODELAY_GROUP : STRING;
    attribute IODELAY_GROUP of idelay_m : label is "aurora";

    signal rst : std_logic;
    signal clk_iserdes_ddr_n : std_logic;

    signal rx_data_del : std_logic;

begin
    
    rst <= not rst_n_i;
    clk_iserdes_ddr_n <= not clk_iserdes_ddr_i;

    idelay_m : IDELAYE2 
        generic map(
            REFCLK_FREQUENCY 	=> 300.0,
            HIGH_PERFORMANCE_MODE 	=> "TRUE",
            IDELAY_VALUE		=> 0,
            DELAY_SRC		=> "IDATAIN",
            IDELAY_TYPE		=> "VAR_LOAD"
        )
        port map(                
            DATAOUT			=> rx_data_del,
            C			    => sys_clk_i,
            CE			    => '0',
            INC			    => '0',
            DATAIN			=> '0',
            IDATAIN			=> rx_data_i,
            LD			    => '1',
            LDPIPEEN		=> '0',
            REGRST			=> '0',
            CINVCTRL		=> '0',
            CNTVALUEIN		=> delay_value_i,
            CNTVALUEOUT		=> delay_value_o
        );
    
    iserdes_m : ISERDESE2 
        generic map(
            DATA_WIDTH     		=> 8, 			
            DATA_RATE      		=> "DDR", 		
            SERDES_MODE    		=> "MASTER", 		
            IOBDELAY	    	=> "IFD",
            DYN_CLK_INV_EN		=> "FALSE", 		
            INTERFACE_TYPE 		=> "NETWORKING"
        ) 	
        port map (                      
            D       		=> '0',
            DDLY     		=> rx_data_del,
            CE1     		=> '1',
            CE2     		=> '1',
            CLK	   		    => clk_iserdes_ddr_i,
            CLKB    		=> clk_iserdes_ddr_n,
            RST     		=> rst,
            CLKDIV  		=> clk_rx_i,
            CLKDIVP  		=> '0',
            OCLK    		=> '0',
            OCLKB    		=> '0',
            DYNCLKSEL    	=> '0',
            DYNCLKDIVSEL  	=> '0',
            SHIFTIN1 		=> '0',
            SHIFTIN2 		=> '0',
            BITSLIP 		=> '0',
            O	 		=> open,
            Q8  			=> rx_data_o(7),
            Q7  			=> rx_data_o(6),
            Q6  			=> rx_data_o(5),
            Q5  			=> rx_data_o(4),
            Q4  			=> rx_data_o(3),
            Q3  			=> rx_data_o(2),
            Q2  			=> rx_data_o(1),
            Q1  			=> rx_data_o(0),
            OFB 			=> '0',
            SHIFTOUT1		=> open,
            SHIFTOUT2 		=> open
        );
        
end behavioral;
