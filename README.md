# Yarr-PCIe Firmware

This firmware was developed specifically for the usage with the [YARR](https://gitlab.cern.ch/YARR/YARR) software package and commercial PCIe FPGA cards. The firmware was designed for the readout of:
- RD53A
- ITkPixV1
- ITkPixV2

## Support

This firmware repository currently supports the following PCIe cards:
- Trenz TEF1001 Rev 1 (Kintex-7 160T)
- [Trenz TEF1001 Rev 2](https://shop.trenz-electronic.de/de/TEF1001-02-B2IX4-A-PCIe-FMC-Carrier-mit-Xilinx-Kintex-7-160T-4-Lane-PCIe-GEN2-DDR3-SODIMM-ECC) (Kintex-7 160T)
- PLDA XpressK7 (Kintex-7 160T)
- PLDA XpressK7 (Kintex-7 325T)
- Xilinx KC705 (Kintex-7 325T)
- [NumatoLab Nereid K7 (Kintex-7 160T)](https://numato.com/product/nereid-kintex-7-pci-express-fpga-development-board/)

## Firmware releases

Bit files for each firmware release can be found here: [https://yarr.web.cern.ch/firmware](https://yarr.web.cern.ch/firmware/)

## How to use

In order to flash the firmware please use the flash script ``script/flash.sh``. It will prompt the user to answer some questions and select the correct firmware based on that, download it from the public URL and start flashing. (Please be sure to source the ``settings64.sh`` of your Vivado installation)

One can also use the flash script to directly flash a specific firmware without downloading via:
```
$ ./flash.sh <bit file name>.bit
```

## Firmware documentation

- [tx-core](/rtl/tx-core/README.md)