import numpy as np
import os
import glob
import argparse
import sys

if __name__=="__main__":
    p = argparse.ArgumentParser()
    p.add_argument("-s", "--syn", help="relative syn/ directory path", default='syn/', type=str)
    p.add_argument("-a", "--add", help="path to file containing text to add", default=None, type=str)
    p.add_argument("-r", "--remove", help="path to file containing text to remove", default=None, type=str)
    p.add_argument("-d", "--debug", help="debug mode (don't write any files)", action="store_true", default=False)
    args = p.parse_args()
    
    glob_path = os.path.join(args.syn, "*/*/*/Manifest.py")
    files = glob.glob(glob_path)
    if len(files) == 0:
        raise FileNotFoundError("Found no Manifest.py files with glob path {}".format(glob_path))
    
    add = None
    remove = None
    
    if args.add is not None:
        if os.path.exists(args.add):
            with open(args.add, 'r') as f:
                add = f.read()
        else:
            raise FileNotFoundError("Could not find file to add {}".format(args.add))
            
    if args.remove is not None:
        if os.path.exists(args.remove):
            with open(args.remove, 'r') as f:
                remove = f.read()
        else:
            raise FileNotFoundError("Could not find file to remove {}".format(args.remove))
        
    if add is None and remove is None:
        sys.exit(0)
        
    for path in files:
        with open(path, 'r') as f:
            data = f.read()
        if remove is not None:
            if remove in data:
                # find & delete section
                idx = data.find(remove)
                data = data[:idx] + data[idx+len(remove):]
        if add is not None:
            data = data + '\n' + add + '\n'
            
        if args.debug:
            print("Writing {}:".format(path))
            print(data)
            print('\n\n')
        else:
            with open(path, 'w+') as f:
                f.write(data)
        
            
        
                
            
    